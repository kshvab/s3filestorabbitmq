const amqp = require("amqplib");
const fs = require("fs");
const unzipper = require("unzipper");
const { logger } = require("./helpers/logger");
const { RABBIT_CONN, RABBIT_EXCHANGE_NAME, SOURCE_ID, DEAD_LETTER_EXCHANGE_VALUE } = require("./config");

const chunkSize = 8;
const afterMessagePause = 160;

logger.info(new Date().toLocaleString() + " STARTING APP!");

Object.defineProperty(Array.prototype, "chunk_inefficient", {
    value: function (chunkSize) {
        var array = this;
        return [].concat.apply(
            [],
            array.map(function (elem, i) {
                return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
            })
        );
    },
});

const rabbitUrl = RABBIT_CONN;
const q = `${RABBIT_EXCHANGE_NAME}.wazett.saveS3DataToDB`;
const deadLetterExchangeValue = DEAD_LETTER_EXCHANGE_VALUE;

workJob();

async function workJob() {
    const zipsList = getZipsList();
    logger.info(new Date().toLocaleString() + "[x] - Zips in folder: " + zipsList.length);
    try {
        for (const zipName of zipsList) {
            await doWithOneZip(zipName);
            pause(4000); //between zips
        }
    } catch (error) {
        console.log(error);
        logger.error(error);
    }
}

async function doWithOneZip(zipName) {
    // Only 1 zip
    logger.info(new Date().toLocaleString() + "------ STARTING ZIP " + zipName);
    console.log("------ STARTING ZIP " + zipName);
    try {
        const oneZipObjectsArr = await readZipFiles("./zips/" + zipName);

        const newoneZipObjectsArr = oneZipObjectsArr.chunk_inefficient(chunkSize);

        console.log("zip chunks count: ", newoneZipObjectsArr.length);
        console.log("first zip size: ", newoneZipObjectsArr[0].length);

        let chunksCounter = 0;
        for (const chunkArr of newoneZipObjectsArr) {
            chunksCounter++;
            logger.info(new Date().toLocaleString() + "Sending CHUNK " + chunksCounter + " of " + newoneZipObjectsArr.length);
            console.log("Sending CHUNK " + chunksCounter + " of " + newoneZipObjectsArr.length);
            await connectMQAndSendMsgs(chunkArr);
            pause(50); //between chunks 4000
        }
        logger.info(new Date().toLocaleString() + "----- HOTOVO ZIP " + zipName);
        console.log("----- HOTOVO ZIP " + zipName);
        return zipName;
    } catch {
        logger.info(new Date().toLocaleString() + "********* Error ZIP " + zipName);
        console.log("********* Error ZIP " + zipName);
        return 1;
    }
}

function getZipsList() {
    const dir = "./zips";
    const files = fs.readdirSync(dir);

    let zipFilesArr = [];

    for (const file of files) {
        zipFilesArr.push(file);
    }
    return zipFilesArr;
}

function connectMQAndSendMsgs(arrOfObjects) {
    // chunkArr
    logger.info(new Date().toLocaleString() + "[x] - Starting SendFunc");
    console.log("[x] - Starting SendFunc");

    let sentCount = 0;
    return new Promise((resolve, reject) => {
        amqp.connect(rabbitUrl)
            .then(function (conn) {
                return conn
                    .createChannel()
                    .then(function (ch) {
                        var msg = "Hello World!";

                        var ok = ch.assertQueue(q, {
                            durable: true,
                            messageTtl: 240000,
                            deadLetterExchange: deadLetterExchangeValue,
                            deadLetterRoutingKey: "dead",
                        });

                        return ok.then(function (_qok) {
                            // NB: `sentToQueue` and `publish` both return a boolean
                            // indicating whether it's OK to send again straight away, or
                            // (when `false`) that you should wait for the event `'drain'`
                            // to fire before writing again. We're just doing the one write,
                            // so we'll ignore it.

                            for (const oneObj of arrOfObjects) {
                                pause(afterMessagePause);
                                if (ch.sendToQueue(q, Buffer.from(oneObj.string))) {
                                    //logger.info("[x] - SENT  " + oneObj.name);
                                    console.log("[x] - SENT  " + oneObj.name);
                                    sentCount++;
                                } else {
                                    logger.info(new Date().toLocaleString() + "!!! NOT SENT  " + oneObj.name);
                                    console.log("!!! NOT SENT  " + oneObj.name);
                                }
                            }
                            //const sendingResult = ch.sendToQueue(q, Buffer.from(msg));
                            //console.log(sendingResult);
                            //.finally((sendingResult) => console.log(sendingResult));

                            return ch.close();
                        });
                    })
                    .finally(function () {
                        conn.close();
                        logger.info(new Date().toLocaleString() + "[x] - SENT  " + sentCount + " of " + arrOfObjects.length);
                        console.log("[x] - SENT  " + sentCount + " of " + arrOfObjects.length);
                        resolve("Ok");
                    });
            })
            .catch((error) => {
                console.log(error);
                reject(error);
            });
    });
}

function readZipFiles(zipPath) {
    console.log(zipPath);
    return new Promise((resolve, reject) => {
        let resultArr = [];
        fs.createReadStream(zipPath)
            .pipe(unzipper.Parse())
            .on("entry", (entry) => {
                const fileName = entry.path;
                if (fileName.includes(".json")) {
                    const type = entry.type; // 'Directory' or 'File'
                    const size = entry.vars.uncompressedSize; // There is also compressedSize;
                    entry.buffer().then((fileContent) => {
                        try {
                            let dataObj = JSON.parse(fileContent.toString());
                            dataObj.sourceId = +SOURCE_ID;

                            strToSend = JSON.stringify(dataObj);
                            const objWithName = {
                                name: fileName,
                                string: strToSend,
                            };
                            resultArr.push(objWithName);
                        } catch {
                            logger.error(new Date().toLocaleString() + "Error parsing data from " + fileName);
                            console.log("Error parsing data from " + fileName);
                        }
                    });
                }
            })
            .promise()
            .then(
                () => {
                    //console.log("done");
                    //console.log(resultArr.length);
                    // Придать фільтр на ДЖСОН
                    //обробить обєкт і посилать
                    logger.info(new Date().toLocaleString() + "Parsed data: " + resultArr.length);
                    console.log("Parsed data: " + resultArr.length);
                    resolve(resultArr);
                },
                (e) => reject(e)
            );
    });
}

function pause(milliseconds) {
    var dt = new Date();
    while (new Date() - dt <= milliseconds) {
        /* Do nothing */
    }
}
