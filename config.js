const dotenv = require("dotenv");
const path = require("path");

const root = path.join.bind(this, __dirname);

dotenv.config({ path: root(".env") });

const SOURCE_ID = process.env.SOURCE_ID;

const RABBIT_CONN = process.env.RABBIT_CONN;
const RABBIT_EXCHANGE_NAME = process.env.RABBIT_EXCHANGE_NAME;
const DEAD_LETTER_EXCHANGE_VALUE = process.env.DEAD_LETTER_EXCHANGE_VALUE;

module.exports = {
    RABBIT_CONN,
    RABBIT_EXCHANGE_NAME,
    SOURCE_ID,
    DEAD_LETTER_EXCHANGE_VALUE,
};
